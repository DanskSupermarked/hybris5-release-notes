+++
title = "Step products can safely share images"
date = 2018-01-24T14:08:26+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-20914" ]
+++

STEP-sourced products with shared images kept losing and recovering their 
images in hybris during the day. This only happens when a product with 
references to the shared image became unapproved while the shared media being 
updated on the product(s). This is no longer a problem.

It should be possible to reuse the images on multiple products even though some 
of the related products are still under approval workflow or product is 
unapproved due to the discontinuity of the product in STEP system.
