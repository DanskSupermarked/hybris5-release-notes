+++
title = "Update Bilka logo"
date = 2018-04-03T09:36:57+02:00
draft = false
releases = [ "704" ]
tickets = [ "COM-21515" ]
+++

The Bilka Web site has a new logo, now without the ".dk". The new logo has also
been uploaded to Bambora checkout.
