+++
title = "Add a blue confetti theme to føtex"
date = 2018-04-30T09:06:43+02:00
draft = false
releases = [ "706" ]
tickets = [ "COM-21824" ]
+++

We've added a new føtex site theme, "foetex-blue", that features a lighter blue 
background and confetti:

{{< img src="com-21824.png" alt="New \"foetex-blue\" theme" >}}
