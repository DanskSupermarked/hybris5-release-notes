+++
title = "Create a product-references mapping in the Import Cockpit"
date = 2018-04-30T08:55:44+02:00
draft = false
releases = [ "705" ]
tickets = [ "COM-21637" ]
+++

Pre-create a mapping for product references in the Import Cockpit so users can 
import product references by their hybris ERP IDs[^1].

[^1]: "SAP article number" dash "base unit of measure"
