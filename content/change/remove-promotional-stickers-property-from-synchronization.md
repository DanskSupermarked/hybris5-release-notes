+++
title = "Remove promotional stickers property from synchronization"
date = 2018-01-23T15:03:57+01:00
draft = false
releases = [ "692" ]
tickets = [ "COM-20863" ]
+++

Business user create promotional stickers quite frequently and they asked 
whether they can attach the stickers directly with products in 
bilkastorefrontProductCatalogOnline, so they do not have to wait for all that 
waiting of synchronization between different catalogs. So this change remove 
the product’s attribute promotionalStickers to be part of synchronization 
process.
