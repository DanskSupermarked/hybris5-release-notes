+++
title = "PLP display dynamic category facet"
date = 2018-01-23T16:26:00+01:00
draft = false
releases = [ "697" ]
tickets = [ "COM-21195" ]
+++

PLP does not display category facet for dynamic category pages. After the fix, 
We should now have category filter/facet on dynamic category pages.

{{< img src="com-21195.png"
    alt="Category facet on dynamic category pages" >}}
