+++
title = "Fix display of *\"Find din Bilka\"* map and time schedules"
date = 2018-01-24T14:31:17+01:00
draft = false
releases = [ "696" ]
tickets = [ "COM-21117" ]
+++

With Christmas theme, when going to "find din bilka":
https://www.bilka.dk/kundeservice/find-din-bilka, the map is not displayed and
when searching for a post code, the map is still not visible and the time
schedules are not aligned.
