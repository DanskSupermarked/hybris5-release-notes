+++
title = "Expose product documents on PDP"
date = 2018-01-25T15:10:22+01:00
draft = false
releases = [ "unreleased" ]
tickets = [ "COM-12076" ]
+++

Expose products' `details` media in a new fold-out on the PDP as either their
`alt` text or a localizable system message in the `productDetails.documents`
namespace.

{{< img src="com-12076-details.jpg"
    alt="Document uploaded to `details` attribute in PCM" >}}

{{< img src="com-12076-docs.png"
    alt="New PDP fold-out displaying media from `details` attribute" >}}

{{< img src="com-12076-no-docs.png"
    alt="New PDP fold-out for empty `details` attribute" >}}
