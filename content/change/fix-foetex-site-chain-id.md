+++
title = "Fix føtex site chain id"
date = 2018-01-24T14:10:56+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-21014" ]
+++

The føtex site was configured with "22" chain id. It has been corrected to
"022".
