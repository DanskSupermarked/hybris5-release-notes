+++
title = "Update DAM production domain"
date = 2018-02-13T12:25:31+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-21059" ]
+++

Sometime in autumn 2017 the DAM production domain changed from 
https://dansk-prod.adobecqms.net to https://dam.dsg.dk. This took a long time 
to discover because we weren't notified of the change and we aggressively cache 
successfully retrieved images; only newly imported products seemed to exhibit 
the issue.

We updated the cache's configuration on December 15 last year so for end users 
the issue appears to have been fully resolved. Now we've also synchronized the 
internal URL displayed in the HMC and cockpit with the new domain so it doesn't 
*look* like our DAM integration is broken. Only for newly imported or 
re-imported products, though: this is primarily an administrative aid so we 
have little reason to fix already imported products.
