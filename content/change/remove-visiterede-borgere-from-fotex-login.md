+++
title = "Remove \"visiterede borgere\" from føtex login"
date = 2018-02-20T08:47:38+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-21282" ]
+++

This text covered the cancel button on smaller devices and directed the user to
a blank page; it is a remnant from early plans for the iPosen-acquisition.

Before | Now
-------|----
{{< img src="com-21282-before.png" alt="Before" >}} | {{< img 
src="com-21282-after.png" alt="Now: special login gone" >}}
