+++
title = "Make Bambora the default payment provider"
date = 2018-02-07T15:26:30+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-20823" ]
+++

Henceforth our default payment provider will be Bambora, not PayEx. This change 
is unexciting in and of itself but is necessary to integrate financing payment 
via Ekspres Bank in the future. As a happy side-effect it will soon allow us to 
address a production error regarding missing orders resulting from browser 
sessions that die between payment and returning to Bilka/føtex. 
