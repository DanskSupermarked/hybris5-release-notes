+++
title = "Shutdown Innometrics"
date = 2018-01-24T14:05:08+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-20927" ]
+++

Part of the clean up after the switch to Adobe's *Dynamic Tag Manager*.
