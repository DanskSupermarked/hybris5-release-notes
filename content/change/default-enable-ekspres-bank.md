+++
title = "Enable Ekspres Bank payment method by default"
date = 2018-04-30T08:52:10+02:00
draft = false
releases = [ "705" ]
tickets = [ "COM-21582" ]
+++

The Ekspres Bank payment method is now enabled by default. It has been
available for some time already, but only because we've routinely manually
enabled it while building confidence in our Bambora payment.
