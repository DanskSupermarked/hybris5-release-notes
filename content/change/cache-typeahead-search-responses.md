+++
title = "Cache *Typeahead* search responses"
date = 2018-01-24T14:13:37+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-20951" ]
+++

The *typeahead* feature, which displays search results in a pop-up, causes a
lot of requests to our search engine. Previously we couldn't cache those
responses and we suspect this put unnecessary load on an already overworked
search engine during Black Friday. Now we cache those responses. This does not
resolve any underlying issues but at least it prevents us from exacerbating the
issue.
