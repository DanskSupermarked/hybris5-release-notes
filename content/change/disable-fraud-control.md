+++
title = "Disable fraud control in favor of 3D Secure"
date = 2018-04-30T08:50:23+02:00
draft = false
releases = [ "705" ]
tickets = [ "COM-21211" ]
+++

Having enabled 3D Secure on Bambora payments, the internal fraud control now 
defaults to disabled.
