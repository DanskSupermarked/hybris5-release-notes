+++
title = "Restore rating stars accidentally hidden on product detail page"
date = 2018-01-24T14:17:37+01:00
draft = false
releases = [ "696" ]
tickets = [ "COM-21166" ]
+++

When we removed the ability for customers to create product reviews in 
COM-19747 we also started hiding empty rating stars to avoid suggesting that 
products without any rating had been rated 0/5. Unfortunately we ended up 
hiding all empty stars, making it look like a product rated 4/5 was in fact 
rated 4/4:

{{< img src="com-21166-before.png"
    alt="Missing empty rating star" >}}

Now we show the missing stars again (and we still hide all stars for unrated 
products):

{{< img src="com-21166-after.png"
    alt="Restored empty rating star" >}}

Search pages did not have this problem.
