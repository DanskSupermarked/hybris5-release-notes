+++
title = "Product Feed Discontinued Date"
date = 2018-01-23T16:28:10+01:00
draft = false
releases = [ "697" ]
tickets = [ "COM-20920" ]
+++

The product attribute `discontinuedDate` is now updated when we process a feed. 
In case of a null discontinued date we remove the value in Hybris when we 
receive a step feed.

Part of the change was also to block any edits to the discontinued attribute 
for any users member of the SuperUserGroup from any Hybris cockpit (HMC, 
PCM...).
