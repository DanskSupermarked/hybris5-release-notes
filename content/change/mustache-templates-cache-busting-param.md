+++
title = "Mustache templates have no cache-busting parameter"
date = 2018-01-19T14:40:35+01:00
draft = false
releases = [ "697" ]
tickets = ["COM-21054"]
+++

Static assets can be cached for a long time as they should not change, when 
they do change we need to reset the cache, we use:

    ?v=[git commit id]

So a new build/deploy will generate a new asset is anything has changed. This 
now works from mustache templates too.
