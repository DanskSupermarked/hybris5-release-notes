+++
title = "Add promotions mapping to import cockpit"
date = 2018-02-05T15:41:55+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-21342" ]
+++

Add new targets for the import cockpit (product promotions, order promotions,
restrictions, promotion price). Allow pre-create mapping between CSV files for
promotion and Hybris item types.
