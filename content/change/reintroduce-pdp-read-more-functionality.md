+++
title = "Reintroduce read-more functionality of product descriptions"
date = 2018-03-23T08:38:16+01:00
draft = false
releases = [ "704" ]
tickets = [ "COM-21608" ]
+++

The main product description on the PDP is very long and used to have some
*read-more* functionality that hid the majority of the text.

During early 2017 this was removed in an attempt to fix a client side issue
with the *add-to-cart* button. That issue has since been resolved but the
*read-more* was never re-implemented. This change adds that back.
