+++
title = "Verify URLs on Facebook"
date = 2018-01-24T14:37:06+01:00
draft = false
releases = [ "698" ]
tickets = [ "COM-21239" ]
+++

We need to add the Facebook app id as a meta tag to our sites in order to use
Facebook insights. Insights lets you view analytics for traffic to your site
from Facebook.

New configurable field added to the HMC for each site called "Facebook Insights
ID" under the WCMS. This value is added to the master.tag file as a meta tag
with the property `fb:app_id`
