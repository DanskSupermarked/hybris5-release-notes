+++
title = "Fix price display multiple *fragtmand* delivery options"
date = 2018-01-24T14:16:09+01:00
draft = false
releases = [ "696" ]
tickets = [ "COM-20718" ]
+++

When a consignment offers *fragtmand* as a delivery option, if there are
multiple options for delivery time and those options have different prices we
displayed the price of the default option instead of the selected option but we
still charged the correct amount. This caused a discrepancy between what we
informed customers and what we charged them. Now we display the correct price.
