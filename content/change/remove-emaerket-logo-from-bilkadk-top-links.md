+++
title = "Remove e-mærket logo from top links"
date = 2018-02-05T14:33:25+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-21225" ]
+++

On tablets in portrait view there is very little room at the top of the page, 
the e-mærket logo cannot be removed using the CMS so this change removes it. A 
new link in the footer will replace it.
