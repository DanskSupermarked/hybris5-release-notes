+++
title = "Convert product texts to STEP-markup during migration"
date = 2018-01-24T14:26:28+01:00
draft = false
releases = [ "696" ]
tickets = [ "COM-19752", "COM-19863", "COM-20568" ]
+++

When running the hybris-to-STEP product migration we now transform all product
texts (names, descriptions, classification attributes) from HTML into what
we've taken to calling *STEP-markup*, a proprietary plain-text format invented
conversion is to retain as much of the original text formatting *as possible*
by Stibo for their STEP system, which powers our PIM. The purpose of this given
the limitations of STEP-markup. For details of this conversion see the tickets
above but some limitations deserve special mention:

- The purpose of the transformation is to preserve the *original* content.
  Hybris being both the only source of this content and the de-facto standard
  output that means we prioritize transformations that, once converted back
  into HTML by STEP, will look approximately like they do today. The downside
  to this is that the content *inside STEP* may not look "correct". This should
  be okay because STEP's 5 text previews all look different anyway.
- STEP does not support embedded hyperlinks, of which we have quite many. We
  work around this in several ways:
    - The migration includes a list of all embedded hyperlinks and their link
      texts per product.
    - Hyperlinks to buying guides get rewritten so they all point to the
      https://www.bilka.dk, since there are very few if any buying guides on
      https://www.føtex.dk.
    - All other internal hyperlinks are removed from the product text.
    - External hyperlinks get rewritten to appear without the *scheme*
      (http://, https://) for sake of appearance, and because a large portion
      of the schemes are http:// for domains that now redirect to https://.
    - We built a tool that can generate a CSV file with heuristics, including
      liveness checks, of all embedded hyperlinks.
- STEP does not support explicit paragraphs so we have to fake them with hard
  line breaks. This is a regression that would require STEP's HTML generator to
  learn how to created explicit paragraphs. The visual impact should be minor.

