+++
title = "Fix unparseable primary images in DAM feed"
date = 2018-01-24T14:10:00+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-20946" ]
+++

When writing the asset export feed for DAM, products with multiple primary
images were unparseable. Now we export them the same as secondary images so DAM
can parse them.

It is probably an error that these even exist but there is nothing we can
easily do to clean up.
