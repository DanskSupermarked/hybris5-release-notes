+++
title = "Improve robustness of order creation"
date = 2018-02-13T12:40:57+01:00
draft = false
releases = [ "701" ]
tickets = [ "COM-20867" ]
+++

In {{< linkrelease 699 >}} we [made Bambora our default payment provider][psp].
Unfortunately this greatly increased the number of double-orders when paying
via MobilePay, to the point where we had to return to PayEx for the time being.

Now we have integrated Bambora's *callback confirmation* so we can rely on
Bambora telling us about successful transactions instead of the user's browser.
In practical terms this means that **hopefully MobilePay payments will no
longer cause double-ordering**; and if so, we can stick with Bambora, and with
this change **orders will no longer be *lost* because of browsers closing after
payment**[^1]. Bambora will try to inform us of a transaction's status hourly
until we acknowledge receipt, for up to 24 hours after which they will stop.

A downside of this change is that hybris is not guaranteed to have an order by
the time the customer returns to our order confirmation page after payment[^2],
in which case we won't have an order number and therefore can't display an
order confirmation. We will look for the order, and if we find it we will
display the confirmation &mdash; which [improved considerably][confirmation]
also in release 699 &mdash; as normally, but if we don't find it we will
instead display a generic confirmation page letting the user know we'll notify
them later:

{{< img src="com-20867.png" alt="Generic unfinished-order \"confirmation\"" >}}

Eventually this integration will also enable us to receive the Ekspres Bank
*loan reference* so we can forward it to SAP ECC for settlement purposes.

[^1]: An approximately weekly occurrence.
[^2]: In-store customers that choose to pay at the till are completely unaffected by this change. All other forms of payment are affected.
[confirmation]: {{< relref "change/enrich-order-confirmation-page-with-order-information.md" >}}
[psp]: {{< relref "change/make-bambora-default-payment-provider.md" >}}
