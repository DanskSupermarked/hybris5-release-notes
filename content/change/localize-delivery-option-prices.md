+++
title = "Localize delivery option prices"
date = 2018-01-24T14:16:57+01:00
draft = false
releases = [ "696" ]
tickets = [ "COM-20719" ]
+++

Delivery option prices used to display in English locale (using period as
decimal separator). They now display in Danish locale (comma decimal separator)
like the other prices.
