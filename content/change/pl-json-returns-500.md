+++
title = "Product listing JSON returns 500"
date = 2018-01-19T15:44:58+01:00
draft = false
releases = [ "697" ]
tickets = [ "COM-20952" ]
+++

Throws an exception/returns status 500. It should now behave like 
https://www.bilka.dk/\*/pl/\*/json?q=
