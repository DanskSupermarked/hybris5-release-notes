+++
title = "Kill the non-responsive property"
date = 2018-01-24T14:14:26+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-20935" ]
+++

Removal of the feature switch that allows the non-responsive front end to be
enabled. This would not work and just breaks the site now.
