+++
title = "Enrich order confirmation page with order information"
date = 2018-02-05T14:49:22+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-20879" ]
+++

We have expanded the order confirmation page with delivery and basket details:

{{< img src="com-20879-phone.png"
    alt="Order confirmation page with order details on phone" >}}

{{< img src="com-20879-desktop.png"
    alt="Order confirmation page with order details on desktop" >}}
