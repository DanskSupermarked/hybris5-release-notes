+++
title = "Øko+ gets 400 Bad Request from hybris"
date = 2018-01-23T16:30:43+01:00
draft = false
releases = [ "694" ]
tickets = [ "COM-20940" ]
+++

Changes for hybris suggested by Gigya:

> 1. Check Your Server-Side Code
>
> If you are making signed server-side requests to either the
> accounts.getAccountInfo[4] or socialize.getUserInfo[5] REST API methods, you
> will need to ensure you are not reading the signatureTimestamp or
> UIDSignature from the response, and also that you are not validating the
> signature.
