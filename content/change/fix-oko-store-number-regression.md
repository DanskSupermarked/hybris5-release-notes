+++
title = "Fix Øko+ store number regression"
date = 2018-03-20T10:37:49+01:00
draft = false
releases = [ "703" ]
tickets = [ "COM-21595" ]
+++

The [Bambora callback integration][callback] in {{< linkrelease 701 >}}
accidentally broke Øko+ store number association. This has been fixed.

[callback]: {{< relref "change/integrate-bambora-callback.md" >}}
