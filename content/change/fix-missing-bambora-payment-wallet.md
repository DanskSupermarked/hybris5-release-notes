+++
title = "Fix missing Bambora MobilePay payment wallet"
date = 2018-04-04T08:43:03+02:00
draft = false
releases = [ "704" ]
tickets = [ "COM-21460" ]
+++

Orders paid with MobilePay via Bambora did not transmit the expected 
`PaymentWallet` to ECC. Now it does.
