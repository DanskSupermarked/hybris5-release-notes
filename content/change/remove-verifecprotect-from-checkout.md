+++
title = "Remove *VerifecProtect* check from checkout"
date = 2018-01-24T14:43:46+01:00
draft = false
releases = [ "698" ]
tickets = [ "COM-21210" ]
+++

Removing the external API call to fraud id during checkout as the company is
closing the service, since we were not using the response during the checkout
flow this has no impact.
