+++
title = "Fix articles not synchronized on storefront"
date = 2018-03-09T15:01:03+01:00
draft = false
releases = [ "700" ]
tickets = [ "COM-21381" ]
+++

Article mapping and synchronization being aborted if there is any vendor
category with more than 2100 products mapped with merchandise category.

There are some category in vendor catalog where we have more than 2100 products
under the category and there are many we will reached this number soon.

Our mapping jobs fails because SQL server cannot have more than 2100 products
searched via 'in clause'. So, we converted it into sub-query where there is no
limit.
