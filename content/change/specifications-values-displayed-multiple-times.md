+++
title = "Specifications values displayed multiple times"
date = 2018-01-23T15:26:24+01:00
draft = false
releases = [ "692" ]
tickets = [ "COM-20861" ]
+++

Many specifications are displayed more than one.

- *Type* is displayed 9 times: 
  https://www.bilka.dk/elektronik/computere/baerbar-computer/hp-zbook-studio-g3-mobile-workstation---15-6---core-i7-6820hq---16-gb-ram---512-gb-ssd—dansk/p/100281710
 
- *Understøttet flash-hukommelse* is displayed 4 times:
  https://www.bilka.dk/elektronik/computere/baerbar-computer/lenovo-320-14ast---14---a6-9220---4-gb-ram---128-gb-ssd/p/100310575

- Additional specifications are displayed multiple times, the values are also 
  displayed double within the same attribute: 
  https://www.bilka.dk/vin-spiritus/al-spiritus/pastis-routin-montania/p/100262543

{{< img src="com-20861.png"
    alt="Drinx duplicate attributes" >}}
