+++
title = "Integrate Flixmedia on Bilka"
date = 2018-03-20T08:21:56+01:00
draft = false
releases = [ "702" ]
tickets = [ "COM-21539" ]
+++

[Flixmedia](https://flixmedia.eu) is an external company that provides
additional product content to a product details page. We have enabled the
service on Bilka's product details pages.

If Flixmedia recognizes the product the user is viewing the service will
automatically enrich the page. We first ask for content in Danish, then fall
back to English, and if Flixmedia finds nothing, nothing else happens.

Here are two examples:

{{< img src="com-21539-iphone.png"
    alt="iPhone 6s showing Danish Flixmedia content" >}}

{{< img src="com-21539-desktop.png"
    alt="Desktop showing English Flixmedia content" >}}
