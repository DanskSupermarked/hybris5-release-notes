+++
title = "Fix `ProductTileConverter` cache annotation"
date = 2018-01-24T14:40:15+01:00
draft = false
releases = [ "698" ]
tickets = [ "COM-21171" ]
+++

There is a `@Cacheable` on a private method in `ProductTileConverter`. This
method will never be cached and as it is used to create every product tile,
caching the public method may help performance.
