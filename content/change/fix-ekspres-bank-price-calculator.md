+++
title = "Fix Ekspres Bank price calculator"
date = 2018-04-03T10:23:37+02:00
draft = false
releases = [ "704" ]
tickets = [ "COM-21693" ]
+++

The Ekspres Bank loan calculator displayed on the PDP when the Ekspres Bank
payment option is enabled, as it was temporarily during Easter, did not
calculate prices at all. This has been fixed.

The Ekspres Bank integration contract had changed without notice since we 
deployed it in Autumn 2017.
