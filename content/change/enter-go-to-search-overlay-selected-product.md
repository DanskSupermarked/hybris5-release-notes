+++
title = "Make Enter-key go to selected product in search overlay"
date = 2018-01-24T14:38:30+01:00
draft = false
releases = [ "698" ]
tickets = [ "COM-21110" ]
+++

When using arrow keys and pressing Enter on the search bar the browser does not
go to the selected item but searches for the name of it. Now Enter goes to the
product page of the selected item.
