+++
title = "Update export job with modified time"
date = 2018-01-19T14:40:26+01:00
draft = false
releases = ["697"]
tickets = ["COM-21091"]
+++

Modified time added to product export job.

{{< img src="com-21091.png"
    alt="Export job modified time" >}}
