+++
title = "Fix missing *blond* color"
date = 2018-01-24T14:42:44+01:00
draft = false
releases = [ "698" ]
tickets = [ "COM-21284" ]
+++

*Blond* color is missing from our style repository. Due to  missing style, we
cannot display blond color in the color facet/filter. This change is to add the
blond color in style repository.
