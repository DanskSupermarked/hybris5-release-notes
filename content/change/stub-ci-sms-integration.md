+++
title = "Stub CI SMS integration"
date = 2018-02-05T15:39:11+01:00
draft = false
releases = [ "699" ]
tickets = [ "COM-21200" ]
+++

Our CI environment used the production SMS integration so we have accidentally 
been sending text messages from our development environments. Now we use a stub 
integration.
