+++
title = "Fix 2017-12-12 breakdown root cause"
date = 2018-01-24T14:11:50+01:00
draft = false
releases = [ "695" ]
tickets = [ "COM-21088" ]
+++

Hybris does not provide configuration for mail server timeout for read and
connection timeout. Which is big problem for the process engine. Process engine
can get stuck if send email request is waiting for infinite timeout.

We now have following two timeout properties with default value of 6 seconds:

    mail.smtp.timeout=6000
    mail.smtp.connectiontimeout=6000
