+++
title = "Improve double-order handling"
date = 2018-04-30T08:47:48+02:00
draft = false
releases = [ "705" ]
tickets = [ "COM-21404" ]
+++

Multiple (duplicate) orders are created in the system (hybris, SAP ECC, etc.)
for the customer placed order.

Sometime, there are multiple orders created for the same transaction/cart due
to the simultaneous processing of the callback for the same transaction. We can
have 2<sup>nd</sup> callback processing for the same transaction in parallel if
the 1<sup>st</sup> callback processing is taking more than 5 secs (10 secs for
now).

We now introduced database lock on the cart so that order processing should be
done for the callback who owns the lock on the cart.
