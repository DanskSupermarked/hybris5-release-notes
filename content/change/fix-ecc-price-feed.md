+++
title = "Fix ECC price feed"
date = 2018-01-24T14:41:34+01:00
draft = false
releases = [ "698" ]
tickets = [ "COM-21213" ]
+++

Hybris pick the oldest valid price for Solr/PDP/PLP and checkout. We should
feed Solr/PDP/PLP and checkout with latest created valid price.

With this fix, we are picking the valid latest created price for calculating
the post promotional prices.
