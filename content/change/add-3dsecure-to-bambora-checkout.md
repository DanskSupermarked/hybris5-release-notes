+++
title = "Allow disabling internal fraud check"
date = 2018-03-05T12:05:58+01:00
draft = false
releases = [ "700" ]
tickets = [ "COM-21211" ]
+++

As part of activating *3-D Secure* and *Secured By Nets* for Visa, Mastercard,
and Dankort when paying through Bambora we have added the possibility to
disable the fraud service on our order process. For now the check remains
active but we can disable it at any time.

By default every order going through the `placeOrder` and
`placeSubscriptionOrder` steps are checked using the `fraudCheckOrderInternal`
step.

To deactivate fraud check for a business process, change the value of the
new configuration key `dsgfulfilmentprocess.fraud.deactivated-for` to one of:

- `placeOrder`
- `placeOrder,placeSubscriptionOrder`
- `placeSubscriptionOrder`

To reactivate the check for all business processes, clear the value or remove
the key.
