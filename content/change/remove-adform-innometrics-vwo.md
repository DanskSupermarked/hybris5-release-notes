+++
title = "Remove Adform, Innometrics, and VWO references"
date = 2018-01-19T14:41:02+01:00
draft = false
releases = ["697"]
tickets = ["COM-20927", "COM-20929", "COM-20930", "COM-20931"]
+++

Remove all traces of Adform, Innometrics, and VWO; configuration and code.
Clean up after shutting down these services.
