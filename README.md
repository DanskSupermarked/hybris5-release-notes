# Release notes for hybris 5-sites

This repository contains the release notes for DSG sites powered by the hybris
5 platform:

- https://www.bilka.dk
- https://www.foetex.dk

The repository is a static Website built with a (*very* fast) static-site
generator called Hugo [[hugo]].

- [Installation](#installation)
    - [Development, content creation](#development-content-creation)
- [Writing and publishing release notes](#writing-and-publishing-release-notes)
    - [Write change document](#write-change-document)
    - [Publish release version](#publish-release-version)
- [Contributing other content](#contributing-other-content)
- [Advice for writers](#advice-for-writers)
    - [What, why, how](#what-why-how)
    - [Remember audience](#remember-audience)
    - [JIRA tickets are not &mdash; and should not be &mdash; release
      notes](#jira-tickets-are-not-and-should-not-be-release-notes)
    - [Not-quite-Markdown](#not-quite-markdown)
    - [Rich copy](#rich-copy)
    - [Change-document checklist](#change-document-checklist)

## Installation

To build the site you need Hugo. Hugo compiles into a static, self-contained
binary, and can be installed in several ways [[hugo-install]]; however, we
*recommend* that you use the included `hugow` wrapper to automatically download
and execute a known-good version, passing all arguments directly to the `hugo`
executable.

To build the site for production, just run Hugo with no arguments:

```sh
$ ./hugow
```

The resulting `public/` directory will be ready for deploy.

### Development, content creation

Hugo comes with a bundled HTTP server and file monitor that are indispensable
for a fast feedback cycle while creating content. Run it with

```sh
$ ./hugow server
```

Hugo will instruct you on what address to view the site.

Pass the `--help` option for details on what else Hugo can do.

## Writing and publishing release notes

Hugo comes with built-in (although slightly limited) support for custom
*taxonomies* [[definition], [documentation]]. We leverage this to group details
for individual changes, called *change documents*, into a `release` taxonomy as
*values* of release version *terms*. The structure can be mapped out like this:

    Release                      <- Taxonomy
      37                         <- Term
        Fix the foo              <- Value
        Frobnicate the ansible   <- Value
      42                         <- Term
        Answer meaning of life   <- Value

Hugo then automatically generates a list of all release versions, and for every
version, a list of every associated change document.

A published release term should include a *release document*, which defines
meta data, via front matter, specific to said release. The relevant front
matter variables are

- `title`: the release version; required; autofilled, don't change
- `date`: the release date in ISO 8601 date format; required

A release document may optionally include a [preamble][preamble].

### Write change document

For any change significant enough to warrant a release note entry the
responsible change author also contributes to this repository a *change
document*.

A change document is a self-contained document that describes a particular
change, or occasionally a set of closely related changes, in isolation from any
other changes that eventually make it into the same release. This lets authors
contribute change documents completely independently of the release planning,
and if a change document is late to the party it will still end up in the
correct release note.

Change documents have a specific structure. Hugo uses an *archetype*
[[archetype]] to create the minimal structure automatically but invoking the
command correctly is a bit tricky:

```sh
$ ./hugow new change/frobnicate-the-ansible.md [--editor <editor>]
```

As an alternative to the canonical Hugo command above, consider using the
included `document-change-that-will` wrapper, which has improved ergonomics and
some pitfall workarounds, and assists with creating uniform change titles. The
equivalent command then becomes

```sh
$ ./document-change-that-will [--edit] frobnicate the ansible
```

Proceed to author your newly created change document in your preferred editor.
The optional arguments illustrated above open either the named editor or your
user-default editor per the `$VISUAL` and `$EDITOR` environment variables.

Be sure to review our [advice for writers](#advice-for-writers).

### Publish release version

Eventually the change described by a change document is scheduled for release
and the governing change document needs to reflect this.

When a new release is planned, identify all change documents describing changes
in said release and adjust their `releases` taxonomy to identify the release.
The next site build will automatically list the new release with all linked
change documents. Any person can do this job, although generally a designated
release manager will be involved at least to coordinate included changes.

Although the above will work as described without any further effort, before
publishing a release version you *should* create a new *release document* as
detailed above. For a given release version `<version>`, create such a document
with the command

```sh
$ ./hugow new releases/<version>/_index.md [--editor <editor>]
```

and adjust the front matter accordingly.

If you have JIRA credentials you can use the `jira-synchronize-fixversion`
script to automatically

1. retrieve all tickets included in the specified release;

1. update all matching change documents;

1. create a new release document with the release date as specified in JIRA;
   and

1. stage &mdash; with user confirmation &mdash; and commit the update.

leaving only `git push origin release-<version>`, followed by a merge request,
to publish the new release.

> The change document author is responsible for either recording correct JIRA
> tickets, so the above tool works, or ensuring their documents are correctly
> classified. The release manager is responsible for creating and assigning
> `fixVersion` fields to JIRA tickets.

## Contributing other content

You are free to contribute content that is not release notes but that is still
relevant to the same [target audience](#remember-audience), such as new
functionality, styling, or ancillary documentation. Just remember that this is
not a general-purpose Website.

## Advice for writers

Writing is *hard*. Here is some advice to help you along.

### What, why, how

If your change document does not explain *what* your change does, your change
document should not exist. This may, in fact, be *okay*: there are probably
changes that simply are not worthwhile to advertise outside the version control
log.

If your change has no motivation, your change itself should not exist either.
It is your responsibility to convince the reader that they should even want the
ansible frobnicated. Not every type of change can be perfectly communicated to
every type of reader, but somebody that can be expected to understand the
domain of the change &mdash; and especially somebody that works with it &mdash;
should also be able to understand the change's reasoning.

It may occasionally be relevant to go into some details about how the change
works, for instance if there are feature switches or limitations.

### Remember audience

What follows here is an approximately prioritized &mdash; but non-exhaustive
&mdash; listing of audiences for our release notes. Our job is to write content
that *helps these people*.

1. Colleagues using hybris. People responsible for site content, fraud
   management, and other things. Less visibly, costumer service agents, who end
   up being the first line of support for a great deal of inquiries that result
   from bugs or new features.

1. Ourselves. The act of writing release notes is extremely helpful for
   identifying the essence (and non-essences) of a change, as well as general
   practice in communication. As an archive, release notes give us some insight
   into our technical advancements that is more accessible than a version
   control log.

1. People paying our bills. Leaders, leaders' leaders, and so on principally to
   the top. In practice only very few of these *actually* read our content but
   some of them do and they have virtually no technical understanding, nor any
   particular interest in changes that are not already high profile. Many of
   these people expect to have "relevant" information spoon-fed without seeking
   it out, ranking them a little lower on this list.

1. Our department in general. As a point of pride we want to be able to show
   off our work. We think this is healthy for motivation and quality assurance.

### JIRA tickets are not &mdash; and should not be &mdash; release notes

Don't just copy content straight from the main ticket; neither title nor body.
Unless you personally wrote the ticket there is a pretty good possibility it is
full of wrong assumptions from well-meaning users that only have a fraction of
the full picture. On the other hand, if you did personally write it there is a
good possibility it is full of notes about exactly what changed and how but
with no mention of why it had to change.

Do not spend effort trying to change this in order to achieve an idealized goal
of publishing tickets as release notes. Not only will you suffocate in the
overwhelming volume of inadequate contributions by other users with no personal
stake in your goal, but that idealized goal simply is not the purpose of ticket
trackers in the first place: their purpose is to facilitate discussion of a
topic with a well-defined scope and to summarize that topic's state.

### Not-quite-Markdown

In true Markdown fashion, Hugo purports to understand Markdown content yet adds
numerous bespoke extensions that have nothing to do with Markdown. That's okay,
in so far as all those extensions address fundamental limitations of vanilla
Markdown that make it unsuitable for this use-case &mdash; just keep in mind
that content is only *mostly* Markdown.

### Rich copy

Take advantage of Markdown, Hugo's Markdown extensions, and HTML to produce
appealing, digestible content. Use pictures when possible, or even a video to
guide users.

Nielsen Norman Group publishes extensive research on [writing for the
Web](https://www.nngroup.com/topic/writing-web/) and you are highly encouraged
to look into this. Some choice articles:

- [Inverted Pyramid: Writing for
  Comprehension](https://www.nngroup.com/articles/inverted-pyramid/)

- [How Little Do Users
  Read?](https://www.nngroup.com/articles/how-little-do-users-read/)

- [Plain Language Is for Everyone, Even
  Experts](https://www.nngroup.com/articles/plain-language-experts/)

- [Legibility, Readability, and Comprehension: Making Users Read Your
  Words](https://www.nngroup.com/articles/legibility-readability-comprehension/)

### Change-document checklist

Remember to go through this list when authoring a new change document.

- Clean up the title, which is based on the file name.

- Do *not* change the `release` taxonomy's default value of `"unreleased"`
  unless you know for certain what version your change will appear in.

- Fill the `tickets` attribute with any relevant ticket keys.

- Spell-check; use `./spellcheck` for a consistent result.

- Run `optipng(1)` or `jpegoptim(1)` on any new images.

- Build the site to verify it works and to preview your rendered change
  document.

[archetype]: https://gohugo.io/content-management/archetypes/
[definition]: https://en.wikipedia.org/wiki/Taxonomy_(general)
[documentation]: https://gohugo.io/content-management/taxonomies/
[hugo-install]: https://gohugo.io/getting-started/installing/
[hugo]: https://gohugo.io/
[preamble]: https://en.oxforddictionaries.com/definition/preamble
